package com.nr4home.dao.powerMeter

import com.nr4home.dao.measurement.ElectricalPowerMeasure
import com.nr4home.repository.fileSystem.FileSystemRepository
import com.nr4home.repository.fileSystem.PowerMeterDBO
import org.springframework.stereotype.Service


@Service
class PowerMeterDao(
        private val fileSystemRepository: FileSystemRepository
) {
    fun findPowerMeters(): List<String> {
        return fileSystemRepository.findElectricalPowerMeter()
    }

    fun storePowerMeters(electricalPowerMeasures: List<ElectricalPowerMeasure>) {
        electricalPowerMeasures.forEach {
            if (fileSystemRepository.readElectricalPowerMeter(it.id) == null) {
                fileSystemRepository.storeElectricalPowerMeter(PowerMeterDBO(it.id, it.producer))
            }
        }
    }

    fun findPowerMeter(id: String): PowerMeter? {
        val powerMeterDBO = fileSystemRepository.readElectricalPowerMeter(id)
        return if (powerMeterDBO != null) {
            PowerMeter(powerMeterDBO.id, powerMeterDBO.producer)
        } else {
            null
        }
    }
}
