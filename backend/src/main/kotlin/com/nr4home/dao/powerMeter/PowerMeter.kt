package com.nr4home.dao.powerMeter

data class PowerMeter(
        val id: String,
        val producer: Boolean
)