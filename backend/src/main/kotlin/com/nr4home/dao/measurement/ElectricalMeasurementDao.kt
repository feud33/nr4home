package com.nr4home.dao.measurement

import com.nr4home.repository.electricalMeasurment.ElectricPowerMeterRepository
import org.springframework.stereotype.Service

@Service
class ElectricalMeasurementDao(
        private val electricPowerMeterRepository: ElectricPowerMeterRepository
) {
    fun read(): List<ElectricalPowerMeasure> {
        val powerDataMap = hashMapOf<String, ElectricalPowerMeasure>()
        electricPowerMeterRepository.read().mapNotNull { line ->
            line.toElectricPowerMeterData()
        }.forEach {
            val existingPowerData = powerDataMap[it.id]
            if (existingPowerData == null) {
                powerDataMap[it.id] = it
            } else {
                powerDataMap[it.id] = ElectricalPowerMeasure(
                        id = existingPowerData.id,
                        producer = existingPowerData.producer,
                        value = existingPowerData.value + it.value
                )
            }
        }
        return powerDataMap.values.toList()
    }

    private fun String.toElectricPowerMeterData(): ElectricalPowerMeasure? {
        val splitLine = this.split('|')
        return if (splitLine.size == 2) {
            try {
                ElectricalPowerMeasure(
                        id = splitLine[0],
                        producer = true,
                        splitLine[1].toInt()
                )
            } catch (e: NumberFormatException) {
                null
            }
        } else {
            null
        }
    }
}