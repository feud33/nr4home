package com.nr4home.dao.measurement

data class ElectricalPowerMeasure(
        val id: String,
        val producer: Boolean,
        val value: Int
)