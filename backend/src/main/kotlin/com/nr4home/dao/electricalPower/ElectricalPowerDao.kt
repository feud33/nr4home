package com.nr4home.dao.electricalPower

import com.nr4home.dao.measurement.ElectricalPowerMeasure
import com.nr4home.repository.fileSystem.ElectricalPowerDetailDBO
import com.nr4home.repository.fileSystem.FileSystemRepository
import com.nr4home.repository.fileSystem.PowerMeterDBO
import com.nr4home.service.DateRange
import org.springframework.stereotype.Service

@Service
class ElectricalPowerDao(
        private val fileSystemRepository: FileSystemRepository
) {

    fun storeDetailElectricalData(electricalPowerMeasures: List<ElectricalPowerMeasure>) {
        electricalPowerMeasures.forEach {
            if (fileSystemRepository.readElectricalPowerMeter(it.id) == null) {
                fileSystemRepository.storeElectricalPowerMeter(PowerMeterDBO(it.id, it.producer))
            }
        }

        electricalPowerMeasures.forEach {
            fileSystemRepository.storeDetailElectricalMeasure(ElectricalPowerDetailDBO(meterId = it.id, value = it.value))
        }
    }

    fun findDetailElectricalPower(electricalPowerMeterId: String, yesterday: DateRange): List<ElectricalPower> {
        return fileSystemRepository.findDetailElectricalPowerData(electricalPowerMeterId, yesterday)
                .map {
                    ElectricalPower(
                            meterId = electricalPowerMeterId,
                            value = it.value,
                            date = it.date
                    )
                }

    }

    fun getCurrentElectricalPower(electricalPowerMeterId: String): List<ElectricalPower> {
        return fileSystemRepository.getCurrentElectricalPowerData(electricalPowerMeterId)
                .map { ElectricalPower(meterId = it.meterId, value = it.value, date = it.date) }
    }

    fun storeDailyElectricalData(electricalPowerMeterId: String, electricalData: List<ElectricalPower>) {
        val electricalPowerDetails = electricalData.map {
            ElectricalPowerDetailDBO(it.meterId, it.value, it.date)
        }
        fileSystemRepository.storeDailyElectricalPowerData(electricalPowerMeterId, electricalPowerDetails)
    }

    fun storeYearlyElectricalData(electricalPowerMeterId: String, electricalData: List<ElectricalPower>) {
        val electricalPowerDetails = electricalData.map {
            ElectricalPowerDetailDBO(it.meterId, it.value, it.date)
        }
        fileSystemRepository.storeYearlyElectricalPowerData(electricalPowerMeterId, electricalPowerDetails)
    }

}