package com.nr4home.dao.electricalPower

import java.time.LocalDateTime

class ElectricalPower(
        val meterId: String,
        val value: Int,
        val unit: Unit = Unit.WATT_HOUR,
        val date: LocalDateTime = LocalDateTime.now()
) {
    enum class Unit(val shortDisplay: String) {
        WATT_HOUR("Wh")
    }
}
