package com.nr4home.dao

class MutableInt(var value: Int = 0) {
    fun add(valueToAdd: Int) {
        value += valueToAdd
    }
}