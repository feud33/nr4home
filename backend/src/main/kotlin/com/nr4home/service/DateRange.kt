package com.nr4home.service

import java.time.LocalDateTime

data class DateRange(
        val start: LocalDateTime,
        val stop: LocalDateTime
)