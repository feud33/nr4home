package com.nr4home.service

import java.time.LocalDateTime

class DateUtil {

    fun yesterday(now: LocalDateTime): DateRange {
        val begin = now.minusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0)
        val stop = now.withHour(0).withMinute(0).withSecond(0).withNano(0)

        return DateRange(begin, stop)
    }

}

fun LocalDateTime.between(dateRange: DateRange): Boolean {
    return (this >= dateRange.start && this < dateRange.stop)
}
