package com.nr4home.service

import com.nr4home.dao.powerMeter.PowerMeter
import com.nr4home.dao.powerMeter.PowerMeterDao
import org.springframework.stereotype.Service

@Service
class PowerMeterService(
        private val powerMeterDao: PowerMeterDao
) {
    fun findPowerMeters(): List<String> {
        return powerMeterDao.findPowerMeters()
    }

    fun findPowerMeter(id: String): PowerMeter? {
        return powerMeterDao.findPowerMeter(id)
    }

}