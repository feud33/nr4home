package com.nr4home.service

import com.nr4home.dao.MutableInt
import com.nr4home.dao.electricalPower.ElectricalPower
import com.nr4home.dao.electricalPower.ElectricalPowerDao
import com.nr4home.dao.measurement.ElectricalMeasurementDao
import com.nr4home.dao.powerMeter.PowerMeterDao
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ElectricalPowerService(
        private val electricalMeasurementDao: ElectricalMeasurementDao,
        private val electricalPowerDao: ElectricalPowerDao,
        private val powerMeterDao: PowerMeterDao

) {
    val byHour = { date: LocalDateTime -> date.withMinute(0).withSecond(0) }
    val byDay = { date: LocalDateTime -> date.withHour(0).withMinute(0).withSecond(0) }

    fun readAndStore() {
        val electricalPowerMeasures = electricalMeasurementDao.read()
        powerMeterDao.storePowerMeters(electricalPowerMeasures)
        electricalPowerDao.storeDetailElectricalData(electricalPowerMeasures)
    }

    fun aggregatebyHourElectricalPowerData(): List<List<ElectricalPower>> {
        val yesterday = yesterday()
        return powerMeterDao.findPowerMeters().map { electricalPowerMeterId ->
            val electricalPowerData = electricalPowerDao.findDetailElectricalPower(electricalPowerMeterId, yesterday)
            val aggregation = electricalPowerData.aggregate(electricalPowerMeterId, byHour)
            electricalPowerDao.storeDailyElectricalData(electricalPowerMeterId, aggregation)

            aggregation
        }
    }


    fun aggregateByDayElectricalPowerData(): List<List<ElectricalPower>> {
        val yesterday = yesterday()
        return powerMeterDao.findPowerMeters().map { electricalPowerMeterId ->
            val electricalPowerData = electricalPowerDao.findDetailElectricalPower(electricalPowerMeterId, yesterday)
            val aggregation = electricalPowerData.aggregate(electricalPowerMeterId, byDay)
            electricalPowerDao.storeYearlyElectricalData(electricalPowerMeterId, aggregation)

            aggregation
        }
    }

    private fun yesterday() = DateUtil().yesterday(LocalDateTime.now())


    private fun List<ElectricalPower>.aggregate(
        electricalPowerMeterId: String,
        byAggregate: (LocalDateTime) -> LocalDateTime
    ): List<ElectricalPower> {
        return this.groupingBy { byAggregate(it.date) }
            .fold({ key, _ -> key to MutableInt() },
                { _, accumulator, element ->
                    accumulator.also { (_, mutableInt) -> mutableInt.add(element.value) }
                })
            .values.toList()
            .map {
                ElectricalPower(
                            meterId = electricalPowerMeterId,
                            value = it.second.value,
                            date = it.first
                )
            }
    }

    fun findCurrentElectricalPower(count: Int): List<ElectricalPower> {
        return powerMeterDao.findPowerMeters().mapNotNull { electricalPowerMeterId ->
            findCurrentElectricalPowerById(electricalPowerMeterId, count)
        }
    }

    fun findCurrentElectricalPowerById(electricalPowerMeterId: String, count: Int): ElectricalPower? {
        val currentElectricalPower = electricalPowerDao.getCurrentElectricalPower(electricalPowerMeterId)
            .takeLast(count)
            .fold(MutableElectricalPower()) { mutableElectricalPower, electricalPowerDetail ->
                mutableElectricalPower.meterId = electricalPowerDetail.meterId
                mutableElectricalPower.value += electricalPowerDetail.value
                mutableElectricalPower.count++
                mutableElectricalPower.date = electricalPowerDetail.date
                mutableElectricalPower
            }
        val meterId = currentElectricalPower.meterId
        val date = currentElectricalPower.date
        return if (meterId != null && date != null) {
            ElectricalPower(
                meterId = meterId,
                value = (currentElectricalPower.value * 60 / currentElectricalPower.count) / 1000,
                date = date
            )
        } else {
            null
        }
    }

    class MutableElectricalPower(
        var meterId: String? = null,
        var value: Int = 0,
        var count: Int = 0,
        var date: LocalDateTime? = null
    )
}