package com.nr4home.repository.fileSystem

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ElectricalPowerDetailDBO(
        val meterId: String,
        val value: Int,
        val date: LocalDateTime = LocalDateTime.now()
) {
    companion object {
        const val dateFormat = "yyyy-MM-dd HH:mm:ss"
    }

    fun toRecord(): String {
        val formattedDate = date.format(DateTimeFormatter.ofPattern(dateFormat))
        return "$formattedDate;$value"
    }
}

fun String.toElectricalPowerDetailDBO(electricalPowerMeterId: String): ElectricalPowerDetailDBO {
    val record = this.split(";")
    return ElectricalPowerDetailDBO(
            meterId = electricalPowerMeterId,
            value = record[1].toInt(),
            date = LocalDateTime.parse(record[0], DateTimeFormatter.ofPattern(ElectricalPowerDetailDBO.dateFormat))
    )
}
