package com.nr4home.repository.fileSystem

import com.nr4home.properties.Properties
import com.nr4home.service.DateRange
import com.nr4home.service.between
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Service
class FileSystemRepository(
        private val fileHelper: FileHelper,
        private val properties: Properties
) {
    val logger: Logger = LoggerFactory.getLogger(FileSystemRepository::class.java)

    companion object {
        const val fileExtension = "csv"
    }

    /*
     ElectricalPowerMeter Storage
    */
    fun readElectricalPowerMeter(electricalPowerMeterId: String): PowerMeterDBO? {
        val filename = electricalPowerMeterFile(electricalPowerMeterId)
        return if (fileHelper.exists(filename)) {
            fileHelper.readLines(filename).first().toElectricalPowerMeter(electricalPowerMeterId)
        } else {
            null
        }
    }

    fun storeElectricalPowerMeter(powerMeterDBO: PowerMeterDBO) {
        val dirname = electricalPowerMeterDir()
        fileHelper.mkdirs(dirname)

        val filename = electricalPowerMeterFile(powerMeterDBO.id)
        fileHelper.writeText(filename, "${powerMeterDBO.toRecord()}\n")
        logger.info("Electrical Power Meter File created : $filename")
    }

    fun findElectricalPowerMeter(): List<String> {
        val filename = electricalPowerMeterDir()
        val fileLines = fileHelper.list(filename)
        return fileLines?.map { name ->
            name.split(".").first()
        } ?: listOf()
    }

    private fun electricalPowerMeterFile(electricalPowerMeterId: String): String {
        return "${electricalPowerMeterDir()}/$electricalPowerMeterId.$fileExtension"
    }

    private fun electricalPowerMeterDir(): String {
        return properties.storage.powerMeterDir
    }


    /*
     ElectricalPowerDetail Storage
    */
    fun storeDetailElectricalMeasure(electricalPowerDetailDBO: ElectricalPowerDetailDBO) {
        val filename = detailElectricalMeasureFile(electricalPowerDetailDBO.meterId, electricalPowerDetailDBO.date)
        if (fileHelper.exists(filename)) {
            fileHelper.appendText(filename, electricalPowerDetailDBO.toRecord() + "\n")
        } else {
            val dirname = detailElectricalMeasureDir(electricalPowerDetailDBO.meterId)
            fileHelper.mkdirs(dirname)
            fileHelper.writeText(filename, electricalPowerDetailDBO.toRecord() + "\n")
            logger.info("Detail Electrical Measure File created : $filename")
        }
    }

    fun findDetailElectricalPowerData(electricalPowerMeterId: String, yesterday: DateRange): List<ElectricalPowerDetailDBO> {
        val detailElectricalMeasurementDir = detailElectricalMeasureDir(electricalPowerMeterId)
        val lines = fileHelper.list(detailElectricalMeasurementDir)?.toList()
        return if (lines != null) {
            lines.map { name ->
                val fileFormattedDate = name.split(".").first()
                LocalDateTime.parse(fileFormattedDate, DateTimeFormatter.ofPattern("yyyyMMddHH"))
            }.filter {
                it.between(yesterday)
            }.map { fileDate ->
                val fileName = detailElectricalMeasureFile(electricalPowerMeterId, fileDate)
                fileHelper.readLines(fileName).map { line ->
                    line.toElectricalPowerDetailDBO(electricalPowerMeterId)
                }
            }.flatten()
        } else {
            listOf()
        }
    }


    fun getCurrentElectricalPowerData(electricalPowerMeterId: String): List<ElectricalPowerDetailDBO> {
        val detailElectricalMeasurementDir = detailElectricalMeasureDir(electricalPowerMeterId)
        val filename = fileHelper.list(detailElectricalMeasurementDir)?.toList()?.sorted()?.last()
        return if (filename != null) {
            fileHelper.readLines("$detailElectricalMeasurementDir/$filename").map {
                it.toElectricalPowerDetailDBO(electricalPowerMeterId)
            }
        } else {
            listOf()
        }
    }


    private fun detailElectricalMeasureFile(electricalPowerMeterId: String, recordDate: LocalDateTime): String {
        val formattedDate = recordDate.format(DateTimeFormatter.ofPattern("yyyyMMddHH"))
        return "${detailElectricalMeasureDir(electricalPowerMeterId)}/$formattedDate.$fileExtension"
    }

    private fun detailElectricalMeasureDir(electricalPowerMeterId: String): String {
        return "${properties.storage.detailMeasureDir}/$electricalPowerMeterId"
    }


    /*
         ElectricalPowerDetail Storage
    */

    fun storeDailyElectricalPowerData(electricalPowerMeterId: String, electricalPowerDetailDBOs: List<ElectricalPowerDetailDBO>) {
        val filename = dailyElectricalMeasureFile(electricalPowerMeterId, electricalPowerDetailDBOs.first().date)
        val fileLines = electricalPowerDetailDBOs.joinToString("\n") { it.toRecord() }
        if (fileHelper.exists(filename)) {
            fileHelper.appendText(filename, fileLines + "\n")
        } else {
            fileHelper.mkdirs(dailyElectricalMeasureDir(electricalPowerMeterId))
            fileHelper.writeText(filename, fileLines + "\n")
        }
        logger.info("Daily Electrical Measure File created : $filename")
    }

    private fun dailyElectricalMeasureFile(electricalPowerMeterId: String, recordDate: LocalDateTime): String {
        val formattedDate = recordDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))
        return "${dailyElectricalMeasureDir(electricalPowerMeterId)}/$formattedDate.$fileExtension"
    }

    private fun dailyElectricalMeasureDir(electricalPowerMeterId: String): String {
        return "${properties.storage.dailyMeasureDir}/$electricalPowerMeterId"
    }


    fun storeYearlyElectricalPowerData(electricalPowerMeterId: String, electricalPowerDetailDBOs: List<ElectricalPowerDetailDBO>) {
        val filename = yearlyElectricalMeasureFile(electricalPowerMeterId, electricalPowerDetailDBOs.first().date)
        val fileLines = electricalPowerDetailDBOs.joinToString("\n") { it.toRecord() }
        if (fileHelper.exists(filename)) {
            fileHelper.appendText(filename, fileLines + "\n")
        } else {
            fileHelper.mkdirs(yearlyElectricalMeasureDir(electricalPowerMeterId))
            fileHelper.writeText(filename, fileLines + "\n")
        }
        logger.info("Yearly Electrical Measure File created : $filename")
    }

    private fun yearlyElectricalMeasureFile(electricalPowerMeterId: String, recordDate: LocalDateTime): String {
        val formattedDate = recordDate.format(DateTimeFormatter.ofPattern("yyyy"))
        return "${yearlyElectricalMeasureDir(electricalPowerMeterId)}/$formattedDate.$fileExtension"
    }

    private fun yearlyElectricalMeasureDir(electricalPowerMeterId: String): String {
        return "${properties.storage.yearlyMeasureDir}/$electricalPowerMeterId"
    }
}

fun String.toElectricalPowerMeter(electricalPowerMeterId: String): PowerMeterDBO {
    return PowerMeterDBO(id = electricalPowerMeterId, producer = this == "+")
}
