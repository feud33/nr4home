package com.nr4home.repository.fileSystem

class PowerMeterDBO(
        val id: String,
        val producer: Boolean
) {
    fun toRecord(): String {
        return if (this.producer) {
            "true"
        } else {
            "false"
        }
    }
}
