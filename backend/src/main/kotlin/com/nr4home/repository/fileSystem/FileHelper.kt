package com.nr4home.repository.fileSystem

import org.springframework.stereotype.Service
import java.io.File

@Service
class FileHelper() {
    fun exists(filename: String) = File(filename).exists()

    fun readLines(filename: String) = File(filename).readLines()

    fun mkdirs(filename: String) = File(filename).mkdirs()

    fun writeText(filename: String, s: String) = File(filename).writeText(s)

    fun appendText(filename: String, s: String) = File(filename).appendText(s)

    fun list(filename: String) = File(filename).list()?.toList()
}


