package com.nr4home

import com.nr4home.api.version.VersionController
import com.nr4home.service.ElectricalPowerService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled

@Configuration
@EnableScheduling
class Scheduler(
        val electricalPowerService: ElectricalPowerService
) {
    val logger: Logger = LoggerFactory.getLogger(VersionController::class.java)

    @Scheduled(fixedRate = 60000, initialDelay = 10000)
    fun readAndStoreElectricPowerData() {
        logger.debug("Read And Store Electric Power Data")
        electricalPowerService.readAndStore()
    }

    @Scheduled(cron = "0 2 0 * * *")
    fun aggregateDailyElectricalPowerData() {
        logger.debug("Aggregate")
        electricalPowerService.aggregateByDayElectricalPowerData()
        electricalPowerService.aggregatebyHourElectricalPowerData()
    }
}

