package com.nr4home.api.powermeter

import com.nr4home.dao.powerMeter.PowerMeter
import com.nr4home.service.PowerMeterService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class PowerMeterController(val powerMeterService: PowerMeterService) {
    @GetMapping("/api/powermeters")
    fun findPowerMeters(): List<String> {
        return powerMeterService.findPowerMeters()
    }

    @GetMapping("/api/powermeter/{id}")
    fun getPowerMeters(@PathVariable id: String): PowerMeter? {
        return powerMeterService.findPowerMeter(id)
    }

}