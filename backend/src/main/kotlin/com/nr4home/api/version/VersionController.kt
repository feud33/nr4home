package com.nr4home.api.version

import com.nr4home.properties.Properties
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class VersionController(val properties: Properties) {
    @GetMapping("/api/version")
    fun version(): Version {
        return Version(properties.version)
    }
}