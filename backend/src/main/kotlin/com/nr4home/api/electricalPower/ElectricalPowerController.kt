package com.nr4home.api.electricalPower

import com.nr4home.dao.electricalPower.ElectricalPower
import com.nr4home.service.ElectricalPowerService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ElectricalPowerController(val electricalPowerService: ElectricalPowerService) {
    @GetMapping("/api/electricalpowers")
    fun getElectricalPowers(@RequestParam count: Int): List<ElectricalPower> {
        return electricalPowerService.findCurrentElectricalPower(count)
    }

    @GetMapping("/api/electricalpower/{id}")
    fun getElectricalPower(@PathVariable id: String, @RequestParam count: Int): ElectricalPower? {
        return electricalPowerService.findCurrentElectricalPowerById(id, count)
    }
}