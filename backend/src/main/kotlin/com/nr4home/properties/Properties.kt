package com.nr4home.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("nr4home")
data class Properties(
        val banner: Banner = Banner(),
        val version: String = "1.0",
        val storage: Storage = Storage(),
        val powerMeter: PowerMeter = PowerMeter()
) {
    data class Banner(
            val location: String = "location"
    )

    data class Storage(
            val dir: String = "dir",
            val powerMeterDir: String = "dir",
            val detailMeasureDir: String = "dir",
            val dailyMeasureDir: String = "dir",
            val yearlyMeasureDir: String = "dir"
    )

    data class PowerMeter(
            val port: String = "COM4"
    )
}