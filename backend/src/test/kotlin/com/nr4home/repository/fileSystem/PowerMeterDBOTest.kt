package com.nr4home.repository.fileSystem

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PowerMeterDBOTest {
    @Test
    fun `should produce record`() {
        val electricalPowerMeterDBO = PowerMeterDBO("id", true)

        val record = electricalPowerMeterDBO.toRecord()

        assertEquals("true", record)
    }
}