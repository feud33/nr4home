package com.nr4home.repository.fileSystem

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ElectricalPowerDetailDBOTest {
    @Test
    fun `should produce record`() {
        val dateTime = LocalDateTime.parse("2021022220", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val electricalPowerDetailDBO = ElectricalPowerDetailDBO("id", 500, dateTime)

        val record = electricalPowerDetailDBO.toRecord()

        assertEquals("2021-02-22 20:00:00;500", record)
    }

    @Test
    internal fun `should map String to ElectricalPowerDetailDBO`() {
        val record = "2021-02-22 20:00:00;500"

        val electricalPowerDetailDBO = record.toElectricalPowerDetailDBO("id")

        assertEquals("id", electricalPowerDetailDBO.meterId)
        assertEquals(LocalDateTime.parse("2021022220", DateTimeFormatter.ofPattern("yyyyMMddHH")), electricalPowerDetailDBO.date)
        assertEquals(500, electricalPowerDetailDBO.value)
    }
}