package com.nr4home.repository.fileSystem

import com.nr4home.properties.Properties
import com.nr4home.service.DateRange
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class FileSystemRepositoryTest {
    @MockK
    lateinit var fileHelper: FileHelper

    @InjectMockKs
    lateinit var fileSystemRepository: FileSystemRepository

    @BeforeEach
    fun setUp() {
        val properties = Properties()
        fileHelper = mockkClass(FileHelper::class)
        fileSystemRepository = FileSystemRepository(fileHelper, properties)
    }

    @Test
    fun `should return power meter`() {
        every { fileHelper.exists(any()) } returns true
        every { fileHelper.readLines(any()) } returns listOf("+", "+")

        val electricalPowerMeterDBO = fileSystemRepository.readElectricalPowerMeter("READER")

        assertNotNull(electricalPowerMeterDBO)
        assertEquals("READER", electricalPowerMeterDBO?.id)
        assertEquals(true, electricalPowerMeterDBO?.producer)
    }

    @Test
    fun `shouldn't return power meter`() {
        every { fileHelper.exists(any()) } returns false

        val electricalPowerMeterDBO = fileSystemRepository.readElectricalPowerMeter("READER")

        assertNull(electricalPowerMeterDBO)
    }

    @Test
    fun `should store ElectricalPowerMeter`() {
        every { fileHelper.mkdirs(any()) } returns true
        every { fileHelper.writeText(any(), any()) } returns Unit

        fileSystemRepository.storeElectricalPowerMeter(PowerMeterDBO("id", true))

        verify(exactly = 1) { fileHelper.mkdirs(any()) }
        verify(exactly = 1) { fileHelper.writeText(any(), any()) }
    }

    @Test
    fun `should store storeDetailElectricalMeasure existing file`() {
        every { fileHelper.exists(any()) } returns true
        every { fileHelper.appendText(any(), any()) } returns Unit
        val now = LocalDateTime.now()
        ElectricalPowerDetailDBO("id", 500, now)

        fileSystemRepository.storeDetailElectricalMeasure(ElectricalPowerDetailDBO("id", 500, now))

        verify(exactly = 1) { fileHelper.appendText(any(), any()) }
    }

    @Test
    fun `should store storeDetailElectricalMeasure not existing file`() {
        every { fileHelper.exists(any()) } returns false
        every { fileHelper.mkdirs(any()) } returns true
        every { fileHelper.writeText(any(), any()) } returns Unit
        val now = LocalDateTime.now()
        ElectricalPowerDetailDBO("id", 500, now)

        fileSystemRepository.storeDetailElectricalMeasure(ElectricalPowerDetailDBO("id", 500, now))

        verify(exactly = 1) { fileHelper.mkdirs(any()) }
        verify(exactly = 1) { fileHelper.writeText(any(), any()) }
    }

    @Test
    fun `shouldn't find Electrical Power Meter`() {
        every { fileHelper.list(any()) } returns null

        val electricalPowerMeter = fileSystemRepository.findElectricalPowerMeter()

        assertTrue(electricalPowerMeter.isEmpty())
    }

    @Test
    fun `should find Electrical Power Meter`() {
        every { fileHelper.list(any()) } returns listOf("READER1.csv", "READER2.csv")

        val electricalPowerMeter = fileSystemRepository.findElectricalPowerMeter()

        assertEquals(2, electricalPowerMeter.size)
        assertTrue(electricalPowerMeter.contains("READER1"))
        assertTrue(electricalPowerMeter.contains("READER2"))
    }

    @Test
    fun `should map to ElectricalPowerMeter with + value`() {
        val stringToMap = "+"

        val electricalPowerMeter = stringToMap.toElectricalPowerMeter("id")

        assertEquals("id", electricalPowerMeter.id)
        assertTrue(electricalPowerMeter.producer)
    }

    @Test
    fun `should map to ElectricalPowerMeter with other value`() {
        val stringToMap = "other"

        val electricalPowerMeter = stringToMap.toElectricalPowerMeter("id")

        assertEquals("id", electricalPowerMeter.id)
        assertFalse(electricalPowerMeter.producer)
    }

    @Test
    fun `should not findDetailElectricalPowerData`() {
        val meterId = "id"
        val startDate = LocalDateTime.parse("2021010200", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val endDate = LocalDateTime.parse("2021010300", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val yesterday = DateRange(startDate, endDate)
        every { fileHelper.list(any()) } returns null

        val detailElectricalPowerData = fileSystemRepository.findDetailElectricalPowerData(meterId, yesterday)

        assertTrue(detailElectricalPowerData.isEmpty())
    }

    @Test
    fun `should findDetailElectricalPowerData`() {
        val meterId = "id"
        val startDate = LocalDateTime.parse("2021010200", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val endDate = LocalDateTime.parse("2021010300", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val yesterday = DateRange(startDate, endDate)
        every { fileHelper.list(any()) } returns listOf("2021010123.csv", "2021010200.csv", "2021010223.csv", "2021010300.csv")
        every { fileHelper.readLines(any()) } returns listOf("2021-01-02 01:00:13;500")

        val detailElectricalPowerData = fileSystemRepository.findDetailElectricalPowerData(meterId, yesterday)

        assertEquals(2, detailElectricalPowerData.size)
        assertEquals("id", detailElectricalPowerData.first().meterId)
        assertEquals(500, detailElectricalPowerData.first().value)
    }

    @Test
    fun `should store Daily Electrical Power Data`() {
        every { fileHelper.exists(any()) } returns false
        every { fileHelper.writeText(any(), any()) } returns Unit
        every { fileHelper.mkdirs(any()) } returns true
        val meterId = "id"
        val electricalPowerDetailDBOs = listOf(ElectricalPowerDetailDBO(meterId, 500, LocalDateTime.now()))

        fileSystemRepository.storeDailyElectricalPowerData(meterId, electricalPowerDetailDBOs)

        verify(exactly = 1) { fileHelper.mkdirs(any()) }
        verify(exactly = 1) { fileHelper.writeText(any(), any()) }
    }

    @Test
    fun `should store Yearly Electrical Power Data`() {
        every { fileHelper.exists(any()) } returns false
        every { fileHelper.writeText(any(), any()) } returns Unit
        every { fileHelper.mkdirs(any()) } returns true
        val meterId = "id"
        val electricalPowerDetailDBOs = listOf(ElectricalPowerDetailDBO(meterId, 500, LocalDateTime.now()))

        fileSystemRepository.storeYearlyElectricalPowerData(meterId, electricalPowerDetailDBOs)

        verify(exactly = 1) { fileHelper.mkdirs(any()) }
        verify(exactly = 1) { fileHelper.writeText(any(), any()) }
    }
}

