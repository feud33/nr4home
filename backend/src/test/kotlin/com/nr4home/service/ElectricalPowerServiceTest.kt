package com.nr4home.service

import com.nr4home.dao.electricalPower.ElectricalPower
import com.nr4home.dao.electricalPower.ElectricalPowerDao
import com.nr4home.dao.measurement.ElectricalMeasurementDao
import com.nr4home.dao.measurement.ElectricalPowerMeasure
import com.nr4home.dao.powerMeter.PowerMeterDao
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ElectricalPowerServiceTest {

    @MockK
    lateinit var electricalMeasurementDao: ElectricalMeasurementDao
    lateinit var electricalPowerDao: ElectricalPowerDao
    lateinit var powerMeterDao: PowerMeterDao

    lateinit var electricalPowerService: ElectricalPowerService

    @BeforeEach
    fun setUp() {
        electricalMeasurementDao = mockkClass(ElectricalMeasurementDao::class)
        electricalPowerDao = mockkClass(ElectricalPowerDao::class)
        powerMeterDao = mockkClass(PowerMeterDao::class)

        electricalPowerService = ElectricalPowerService(electricalMeasurementDao, electricalPowerDao, powerMeterDao)
    }

    @Test
    internal fun `should read and store`() {
        every { electricalMeasurementDao.read() } returns listOf(ElectricalPowerMeasure("id", true, 500))
        every { electricalPowerDao.storeDetailElectricalData(any()) } returns Unit
        every { powerMeterDao.storePowerMeters(any()) } returns Unit

        electricalPowerService.readAndStore()

        verify(exactly = 1) { electricalPowerDao.storeDetailElectricalData(any()) }
        verify(exactly = 1) { powerMeterDao.storePowerMeters(any()) }
    }

    @Test
    internal fun `should aggregate Hourly Electrical Power Data`() {
        every { powerMeterDao.findPowerMeters() } returns listOf("READER")
        every { electricalPowerDao.findDetailElectricalPower(any(), any()) } returns listOf(
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 20:00", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            ),
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 20:01", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            ),
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 21:20", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            )
        )
        every { electricalPowerDao.storeDailyElectricalData(any(), any()) } returns Unit

        val aggregateElectricalPowerData = electricalPowerService.aggregatebyHourElectricalPowerData()

        assertEquals(1, aggregateElectricalPowerData.size)
        assertEquals(2, aggregateElectricalPowerData.first().size)
        assertEquals("READER", aggregateElectricalPowerData.first().first().meterId)
        assertEquals(1000, aggregateElectricalPowerData.first().first().value)
    }

    @Test
    internal fun `should aggregate Daily Electrical Power Data`() {
        every { powerMeterDao.findPowerMeters() } returns listOf("READER")
        every { electricalPowerDao.findDetailElectricalPower(any(), any()) } returns listOf(
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 20:00", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            ),
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 20:01", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            ),
            ElectricalPower(
                meterId = "id",
                value = 500,
                date = LocalDateTime.parse("20210202 21:20", DateTimeFormatter.ofPattern("yyyyMMdd HH:mm"))
            )
        )
        every { electricalPowerDao.storeYearlyElectricalData(any(), any()) } returns Unit

        val aggregateElectricalPowerData = electricalPowerService.aggregateByDayElectricalPowerData()

        assertEquals(1, aggregateElectricalPowerData.size)
        assertEquals(1, aggregateElectricalPowerData.first().size)
        assertEquals("READER", aggregateElectricalPowerData.first().first().meterId)
        assertEquals(1500, aggregateElectricalPowerData.first().first().value)
    }

    @Test
    internal fun `findCurrentElectricalPower should return empty list because of no results`() {
        every { powerMeterDao.findPowerMeters() } returns listOf("READER")
        every { electricalPowerDao.getCurrentElectricalPower(any()) } returns listOf()

        val currentElectricalPowers = electricalPowerService.findCurrentElectricalPower(2)

        assertEquals(0, currentElectricalPowers.size)
    }

    @Test
    internal fun `findCurrentElectricalPower should return aggregated list`() {
        val now = LocalDateTime.now()
        val oneMinuteAgo = LocalDateTime.now().minusMinutes(1)
        val twoMinutesAgo = LocalDateTime.now().minusMinutes(2)

        every { powerMeterDao.findPowerMeters() } returns listOf("R1")
        every { electricalPowerDao.getCurrentElectricalPower(any()) } returns listOf(
                ElectricalPower(meterId = "R1", value = 2000, date = twoMinutesAgo),
                ElectricalPower(meterId = "R1", value = 1500, date = oneMinuteAgo),
                ElectricalPower(meterId = "R1", value = 500, date = now),
        )

        val currentElectricalPowers = electricalPowerService.findCurrentElectricalPower(2)

        assertEquals(1, currentElectricalPowers.size)
        assertEquals("R1", currentElectricalPowers.first().meterId)
        assertEquals(60, currentElectricalPowers.first().value)
        assertEquals(ElectricalPower.Unit.WATT_HOUR, currentElectricalPowers.first().unit)
        assertEquals(now, currentElectricalPowers.first().date)
    }

    @Test
    internal fun `findCurrentElectricalPower should return aggregated reduce list`() {
        val now = LocalDateTime.now()
        val oneMinuteAgo = LocalDateTime.now().minusMinutes(1)
        val twoMinutesAgo = LocalDateTime.now().minusMinutes(2)

        every { powerMeterDao.findPowerMeters() } returns listOf("R1")
        every { electricalPowerDao.getCurrentElectricalPower(any()) } returns listOf(
                ElectricalPower(meterId = "R1", value = 2000, date = twoMinutesAgo),
                ElectricalPower(meterId = "R1", value = 1500, date = oneMinuteAgo),
                ElectricalPower(meterId = "R1", value = 500, date = now),
        )

        val currentElectricalPowers = electricalPowerService.findCurrentElectricalPower(4)

        assertEquals(1, currentElectricalPowers.size)
        assertEquals("R1", currentElectricalPowers.first().meterId)
        assertEquals(80, currentElectricalPowers.first().value)
        assertEquals(ElectricalPower.Unit.WATT_HOUR, currentElectricalPowers.first().unit)
        assertEquals(now, currentElectricalPowers.first().date)
    }
    // Revoir les méthodes qui ne retournent rien
    //  Faire la purge
}

