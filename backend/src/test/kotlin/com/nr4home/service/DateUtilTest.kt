package com.nr4home.service

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DateUtilTest {
    @Test
    fun `yesterday all my troubles seemed so far away`() {
        val dateTime = LocalDateTime.parse("2021020220", DateTimeFormatter.ofPattern("yyyyMMddHH"))

        val yesterday = DateUtil().yesterday(dateTime)

        assertEquals("2021020100", yesterday.start.format(DateTimeFormatter.ofPattern("yyyyMMddHH")))
        assertEquals("2021020200", yesterday.stop.format(DateTimeFormatter.ofPattern("yyyyMMddHH")))
    }

    @Test
    fun `in between days`() {
        val dateTime = LocalDateTime.parse("2021020220", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val startDateTime = LocalDateTime.parse("2021020100", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val stopDateTime = LocalDateTime.parse("2021020200", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        val yesterday = DateUtil().yesterday(dateTime)

        assertTrue(startDateTime.between(yesterday))
        assertFalse(stopDateTime.between(yesterday))
    }
}
