package com.nr4home

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Nr4homeApplicationTests {

    @Test
    fun test() {
        val now = LocalDateTime.now()
        val begin = now.minusDays(1).withHour(0).withMinute(0).withSecond(0)
        val end = now.withHour(0).withMinute(0).withSecond(0)


        val dateTime = LocalDateTime.parse("2021022220", DateTimeFormatter.ofPattern("yyyyMMddHH"))
        assertEquals(false, dateTime > begin && dateTime < end)
    }
}
