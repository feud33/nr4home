package com.nr4home.dao

import com.nr4home.dao.measurement.ElectricalMeasurementDao
import com.nr4home.repository.electricalMeasurment.ElectricPowerMeterRepository
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ElectricalMeasurementDaoTest {
    @MockK
    lateinit var electricPowerMeterRepository: ElectricPowerMeterRepository

    private lateinit var electricalMeasurementDao: ElectricalMeasurementDao

    @BeforeEach
    fun setUp() {
        electricPowerMeterRepository = mockkClass(ElectricPowerMeterRepository::class)
        electricalMeasurementDao = ElectricalMeasurementDao(electricPowerMeterRepository)
    }

    @Test
    fun `should return empty list for empty result (trouble)`() {
        every { electricPowerMeterRepository.read() } returns listOf()

        val read = electricalMeasurementDao.read()

        assertTrue(read.isEmpty())
    }

    @Test
    fun `should return a 2 entry for single line result`() {
        every { electricPowerMeterRepository.read() } returns listOf("ID|1000")

        val electricPowerMeterData = electricalMeasurementDao.read()

        assertEquals(1, electricPowerMeterData.size)
        assertEquals("ID", electricPowerMeterData.first().id)
        assertEquals(true, electricPowerMeterData.first().producer)
        assertEquals(1000, electricPowerMeterData.first().value)
    }

    @Test
    fun `should return a 3 entry for two lines result`() {
        every { electricPowerMeterRepository.read() } returns listOf(
                "ID|1000",
                "ID2|500",
                "ID|500")
        val electricPowerMeterData = electricalMeasurementDao.read()

        assertEquals(2, electricPowerMeterData.size)
        assertEquals("ID2", electricPowerMeterData[0].id)
        assertEquals(true, electricPowerMeterData[0].producer)
        assertEquals(500, electricPowerMeterData[0].value)
        assertEquals("ID", electricPowerMeterData[1].id)
        assertEquals(true, electricPowerMeterData[1].producer)
        assertEquals(1500, electricPowerMeterData[1].value)
    }

    @Test
    fun `should return an empty list because of bad result`() {
        every { electricPowerMeterRepository.read() } returns listOf("ID|-|ddddd1000")
        val electricPowerMeterData = electricalMeasurementDao.read()

        assertEquals(0, electricPowerMeterData.size)
    }
}