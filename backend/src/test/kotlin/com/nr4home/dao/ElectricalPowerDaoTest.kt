package com.nr4home.dao

import com.nr4home.dao.electricalPower.ElectricalPower
import com.nr4home.dao.electricalPower.ElectricalPowerDao
import com.nr4home.dao.measurement.ElectricalPowerMeasure
import com.nr4home.repository.fileSystem.ElectricalPowerDetailDBO
import com.nr4home.repository.fileSystem.FileSystemRepository
import com.nr4home.service.DateRange
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class ElectricalPowerDaoTest {
    @MockK
    lateinit var fileSystemRepository: FileSystemRepository

    lateinit var electricalPowerDao: ElectricalPowerDao

    @BeforeEach
    fun setUp() {
        fileSystemRepository = mockkClass(FileSystemRepository::class)
        electricalPowerDao = ElectricalPowerDao(fileSystemRepository)
    }

    @Test
    fun `should store detail electrical data`() {
        every { fileSystemRepository.readElectricalPowerMeter(any()) } returns null
        every { fileSystemRepository.storeElectricalPowerMeter(any()) } returns Unit
        every { fileSystemRepository.storeDetailElectricalMeasure(any()) } returns Unit
        val electricalPowerMeasures = listOf(
                ElectricalPowerMeasure("id", true, 500),
                ElectricalPowerMeasure("id2", false, 1000)
        )

        electricalPowerDao.storeDetailElectricalData(electricalPowerMeasures)

        verify(exactly = 2) { fileSystemRepository.storeElectricalPowerMeter(any()) }
        verify(exactly = 2) { fileSystemRepository.storeDetailElectricalMeasure(any()) }
    }

    @Test
    fun `should find detail electrical data`() {
        val electricalPowerMeterId = "id"
        val now = LocalDateTime.now()
        val yesterday = DateRange(now, now)
        val detailElectricalDataDBO = listOf(
                ElectricalPowerDetailDBO("id", 500, now))
        every { fileSystemRepository.findDetailElectricalPowerData(electricalPowerMeterId, yesterday) } returns detailElectricalDataDBO

        val detailElectricalData = electricalPowerDao.findDetailElectricalPower("id", yesterday)

        assertEquals(1, detailElectricalData.size)
        assertEquals("id", detailElectricalData.first().meterId)
        assertEquals(500, detailElectricalData.first().value)
        assertEquals(now, detailElectricalData.first().date)
    }

    @Test
    fun `should store Daily Electrical Data`() {
        val meterId = "id"
        val now = LocalDateTime.now()
        val electricalData = listOf(ElectricalPower(
                meterId = meterId,
                value = 500,
                unit = ElectricalPower.Unit.WATT_HOUR,
                date = now))
        every { fileSystemRepository.storeDailyElectricalPowerData(any(), any()) } returns Unit

        electricalPowerDao.storeDailyElectricalData(meterId, electricalData)

        verify(exactly = 1) { fileSystemRepository.storeDailyElectricalPowerData(any(), any()) }
    }

    @Test
    fun `should store Yearly Electrical Data`() {
        val meterId = "id"
        val now = LocalDateTime.now()
        val electricalData = listOf(ElectricalPower(
                meterId = meterId,
                value = 500,
                unit = ElectricalPower.Unit.WATT_HOUR,
                date = now))
        every { fileSystemRepository.storeYearlyElectricalPowerData(any(), any()) } returns Unit

        electricalPowerDao.storeYearlyElectricalData(meterId, electricalData)

        verify(exactly = 1) { fileSystemRepository.storeYearlyElectricalPowerData(any(), any()) }
    }
}
