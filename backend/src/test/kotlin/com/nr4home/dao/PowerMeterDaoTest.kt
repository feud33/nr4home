package com.nr4home.dao

import com.nr4home.dao.measurement.ElectricalPowerMeasure
import com.nr4home.dao.powerMeter.PowerMeterDao
import com.nr4home.repository.fileSystem.FileSystemRepository
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkClass
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PowerMeterDaoTest {
    @MockK
    lateinit var fileSystemRepository: FileSystemRepository

    lateinit var powerMeterDao: PowerMeterDao

    @BeforeEach
    fun setUp() {
        fileSystemRepository = mockkClass(FileSystemRepository::class)
        powerMeterDao = PowerMeterDao(fileSystemRepository)
    }

    @Test
    fun `should find electrical meters`() {
        every { fileSystemRepository.findElectricalPowerMeter() } returns listOf("OK")

        val electricalPowerMeters = powerMeterDao.findPowerMeters()

        assertEquals(1, electricalPowerMeters.size)
        assertEquals("OK", electricalPowerMeters.first())
    }

    @Test
    fun `should store electrical meters`() {
        every { fileSystemRepository.readElectricalPowerMeter(any()) } returns null
        every { fileSystemRepository.storeElectricalPowerMeter(any()) } returns Unit
        val electricalPowerMeasures = listOf(
                ElectricalPowerMeasure("id", true, 500),
                ElectricalPowerMeasure("id2", false, 1000)
        )

        powerMeterDao.storePowerMeters(electricalPowerMeasures)

        verify(exactly = 2) { fileSystemRepository.storeElectricalPowerMeter(any()) }
    }


}
