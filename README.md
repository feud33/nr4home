# NR4Home

## Introduction

I installed solar panels in 2019 on the roof of my house. I limited the production capability (arround 1000Wp) because I
want to self-consume the electricity. I produce without reselling the excess. When you look more closely, increasing my
production is not necessarily interesting because I will produce more electricity but at times when I do not necessarily
need it (during the day, when I am not home). This surplus will be donated to the network free of charge.

The objective of this project is to make it possible to adapt the electricity consumption of a house to its photovoltaic
production. For example, I would like to run the washing machine or the heater water when there is excess production
during the day, otherwise keep using off-peak hours.

## How to run

### Production mode

Arduino : you have to flash the arduino with the C ino code. It'll re-run automatically when java app start to read data

Raspberry : you can make a shell script to launch the application

```
#!/bin/sh

LOG_FILE="/tmp/nr4home.log"
echo "nr4Home launch" >$LOG_FILE
ls ~pi/*.jar >>$LOG_FILE
/usr/bin/java --version >>$LOG_FILE
nohup java -jar ~pi/nr4Home-0.0.1.jar --spring.profiles.active=prod &
echo "nr4Home launched" >>$LOG_FILE
```

In /etc/rc.local add the following line the run automatically the shell script

```
su -c ~pi/nr4home.sh - pi
```

### Development mode

you can run the application to test api. In this case juste run

```
java -jar nr4Home-0.0.1.jar
```

In that case, Serial port will not be opened but the api will get information from repository. In that case, data will
be stored in C:/tmp/nr4home (see application.properties)

user data-sample.zip to get data sample.

## architecture pattern

| Measure module |
|:--------------|
| Arduino (C)|

| Data storage module |
|:--------------|
| Raspberry + kotlin + database ? |

| API Server |
|:--------------|
| Raspberry + kotlin + spring stack |

| Front end |
|:--------------|
| Raspberry + vue.js |

| Decision module |
|:--------------|
| Raspberry + kotlin + spring stack |

### Measure module

[Measure module](iot/Pulse)
* arduino nano or equivalent
* dds5188 or equivalent
* C language

_Electrical connections must be made by an electrician_ 

I decided to use a DDS5188 meter to put in my home electrical panel. I could also use an orno counter. This so-called pulse meter records the current flowing through it. Difficult to find documentation on this component until I understand that it works the same as the Orno meter. In fact, it does not send an electrical pulse every time it measures a watt has been consumed. It only closes its measure circuit. It therefore behaves like a common push button. Reading the info is therefore very simple with an arduino and a pull-up resistor. [Further information on carnetdumaker.net](https://www.carnetdumaker.net/articles/mesurer-la-consommation-electrique-de-latelier-avec-un-sous-compteur-impulsions-et-une-carte-arduino-genuino/)

For my part, I used the pull-up resistor on board the arduino nano.

I write on the USB output which also serves as the power supply for my arduino uno.

The arduino sends information on the serial port :

```
## Electric Meter setup
##  - send result period : 60000 ms
## Electric meter : READER1
##  - pulse duration : 90 ms
##  - pulse power : 500 mwh
## Electric meter : READER2
##  - Electric meter consumption : +
##  - pulse duration : 90 ms
##  - pulse power : 500 mwh
## Electric meter : READER3
##  - Electric meter consumption : +
##  - pulse duration : 90 ms
##  - pulse power : 500 mwh
READER1|500
READER2|0
READER3|0
READER1|1000
READER2|0
READER3|0
```

### Data storage module

[Data storage module](backend)
* Kotlin + springboot

The goal of this module is to read arduino measure and store it in a local repository.

The springboot scheduler runs the following tasks according to different frequencies :
  - the read and the storage electrical powerData from arduino
  - the aggregation byHour and byDay of the electrical powerData
  - the deletion of old data

I decided the store data with csv files directly in the filesystem. Indeed, the programme runs on a raspberry Pi 3 (+ api server et frontend GUI) and I was not sure that the Pi was able to run a database.
More over, I use a spreadsheet to make further analysis and graphs.

### Server api
* Kotlin + springboot

The stored data is reached by an API. For example :
- GET localhost:8085/api/version
- GET localhost:8085/api/powermeters
- GET localhost:8085/api/powermeter/{ID}
- GET localhost:8085//api/electricalpowers?count=n
 
### Frontend

### Decision module
