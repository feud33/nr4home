/***********************************************************************************
 measure the meter pulse duraction

 Plug digital arduino pins to + electric din meters screw
 Plug GND to - first din meter screw, then plug - to other din meters - screw
 
 Configuration below :                                                             */

byte electricMetersPin = 2; // pin number on arduino board
unsigned long electricMetersPulseStart = 0; 
unsigned long electricMetersPulseStop= 0;

/***********************************************************************************/

// misc data
bool isPulseAlreadyOn=false;

void setup() {
  Serial.begin(9600);

  Serial.println("## Electric Meter setup");
  pinMode(electricMetersPin, INPUT_PULLUP);
}


/*================================*/
void loop() {
  if( isPulseChangeToOn()) {
    electricMetersPulseStart = millis(); 
  }

  if( isPulseChangeToOff()) {
    electricMetersPulseStop = millis();
    sendResult();
  }
}
/*================================*/



bool isPulseChangeToOn() {
  bool isPulseDetected = (digitalRead(electricMetersPin) == LOW);
  if( isPulseDetected && !isPulseAlreadyOn ) {
    delay(5);
    isPulseAlreadyOn=true;
    return true;
  } else {
    return false;
  }
}

bool isPulseChangeToOff() {
  bool isPulseDetected = (digitalRead(electricMetersPin) == LOW);
  if( !isPulseDetected && isPulseAlreadyOn ) {
    delay(5);
    isPulseAlreadyOn=false;
    return true;
  } else {
    return false;
  }
}

void sendResult() {
  Serial.println(electricMetersPulseStop - electricMetersPulseStart );
}
