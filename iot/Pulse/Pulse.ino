/***********************************************************************************
 Send the consumed milliwats every minute with ORNO or DDS5188 DIN Meter

 Plug digital arduino pins to + electric din meters screw
 Plug GND to - first din meter screw, then plug - to other din meters - screw
 
 Configuration below :                                                             */

#define PROD
#ifdef PROD
  #define SEND_RESULT_PERIOD 60000  // Every minute
#else
  #define SEND_RESULT_PERIOD 10000
#endif

#define NUMBER_OF_ELECTRIC_METERS 5
byte electricMetersPin[NUMBER_OF_ELECTRIC_METERS] = {2, 3, 4, 5, 6}; // pin number on arduino board
const char* electricMeterId[NUMBER_OF_ELECTRIC_METERS] = {"METER1", "METER2", "METER3"}; // meter id
unsigned short electricMetersPulseDuration[NUMBER_OF_ELECTRIC_METERS] = {95, 95, 95, 95, 95}; // pulse duration in ms, 80 ms in the documentation, but between 87 et 93 IRL
unsigned short electricMetersPower[NUMBER_OF_ELECTRIC_METERS] = {500, 500, 500, 500, 500}; // one pulse every 500 mw, read the meter doc

/***********************************************************************************/

// electric meters data
unsigned long electricMetersMillis[NUMBER_OF_ELECTRIC_METERS];
unsigned int electricMetersPulseCount[NUMBER_OF_ELECTRIC_METERS];

// misc data
unsigned long sendResultMillis=0;

void setup() {
  Serial.begin(9600);

  Serial.println("## Electric Meter setup");
  Serial.print("##  - send result period : ");
  Serial.print(SEND_RESULT_PERIOD);
  Serial.println(" ms");
  
  for(int i=0;i<NUMBER_OF_ELECTRIC_METERS; i++) {
    electricMetersMillis[i]=0;
    electricMetersPulseCount[i]=0;
    pinMode(electricMetersPin[i], INPUT_PULLUP);

    Serial.print("## Electric meter : ");
    Serial.println(electricMeterId[i]);
    Serial.print("##  - pulse duration : ");
    Serial.print(electricMetersPulseDuration[i]);
    Serial.println(" ms");
    Serial.print("##  - pulse power : ");
    Serial.print(electricMetersPower[i]);
    Serial.println(" mwh");
  }
}


/*================================*/
void loop() {
  for(int i=0;i<NUMBER_OF_ELECTRIC_METERS; i++) {
    if( isPulseInProgress(i) ) {
      countElectricPulse(i);
    }
  }


  if( isItTimeToSendResult() ) {
    sendResult();
  }
}
/*================================*/



bool isPulseInProgress(int counterId) {
  bool pulseDetected = (digitalRead(electricMetersPin[counterId]) == LOW);
  bool pulseDurationExceeded = (millis() - electricMetersMillis[counterId] > electricMetersPulseDuration[counterId]);
  if (pulseDurationExceeded && pulseDetected) {
    electricMetersMillis[counterId]=millis();
    return true;
  } else {
    return false;
  }
}

void countElectricPulse(int counterId) {
  electricMetersPulseCount[counterId]++;
}

bool isItTimeToSendResult() {
  if (millis() - sendResultMillis > SEND_RESULT_PERIOD) {
    sendResultMillis+=SEND_RESULT_PERIOD;
    return true;
  } else {
    return false;
  }
}

void sendResult() {
  for(int i=0;i<NUMBER_OF_ELECTRIC_METERS; i++) {
    Serial.print(electricMeterId[i]);
    Serial.print("|");
    Serial.println(electricMetersPulseCount[i]*electricMetersPower[i]);
    electricMetersPulseCount[i]=0;
  }
}
