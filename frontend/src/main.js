import {createApp} from 'vue'
import App from './App.vue'
import 'boosted/dist/css/boosted.css'
import 'boosted/dist/css/boosted-grid.css'
import 'boosted/dist/css/boosted-reboot.css'
import 'boosted/dist/css/boosted-rtl.css'
import 'boosted/dist/js/boosted.bundle.js'
import 'boosted/dist/js/boosted.js'
import messages from './i18n.js'
import {createI18n} from 'vue-i18n'

const i18n = createI18n({
    locale: 'fr',
    messages
})

createApp(App)
    .use(i18n)
    .mount('#app')