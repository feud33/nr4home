const webpack = require("webpack");
module.exports = {
  lintOnSave: false,
  outputDir: 'build/dist',
  publicPath: "/",

  // Pour permettre la redirection sur le serveur back.
  devServer: {
    host: 'localhost',
    port: 8080,
    disableHostCheck: true,
    https: false,
    proxy: {
      "/api": {
        target: 'http://localhost:8085',
        //pathRewrite: { '^/api': ''},
        changeOrigin: true,
        secure: false,
        headers: {
          https: false,
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': '*',

        }
      }
    },
    headers: {
      https: false,
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
        jQuery: 'jquery'
      })
    ]
  }
};

