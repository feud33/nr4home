plugins {
    base
}

repositories {
    mavenLocal()
}

fun argsWith(vararg args: String): List<String> =
    if (org.apache.tools.ant.taskdefs.condition.Os.isFamily(org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS)) {
        listOf("cmd", "/c") + args.asList()
    } else {
        args.asList()
    }

val npmInstall = task<Exec>("npmInstall") {

    description = "Installs all the Javascript dependencies of project."

    inputs.files("$projectDir/package.json")
    outputs.dir("$projectDir/node_modules")
    outputs.upToDateWhen {
        file("$projectDir/node_modules").exists()
    }
    commandLine(
        argsWith(
            "npm",
            "install",
            "--quiet",
            "--registry=https://artifactory.si.francetelecom.fr/api/npm/virt-pfs-myshop-npm/",
            "--strict-ssl=true",
            "--cafile=ca-certificates.crt"
        )
    )
}

// La task npmInstall crée un repertoire node_modules qui n'est pas
// dans le repertoire de build, donc il faut enregistrer la task cleanNpmInstall
// à la task clean du projet pour que le repertoire node_modules soit supprimé sur
// `gradle clean`
val cleanNpmInstall = tasks.named("cleanNpmInstall")

val runTests = task("runTests", Exec::class) {

    description = "Run front jest unit test."
    commandLine(
        argsWith(
            "npm",
            "run-script",
            "test"
        )
    )
}


val buildFrontend = task("buildFrontend", Exec::class) {
    description = "Build frontend static"

    dependsOn(npmInstall)
    commandLine(
        argsWith(
            "npm",
            "run-script",
            "build"
        )
    )
}


val runServe = task("runServe", Exec::class) {

    description = "Run front serve in local."

    dependsOn(npmInstall)
/*    commandLine(argsWith(
        "npm",
        "run-script",
        "serve"
    ))
 */
}

tasks {
    build {
        dependsOn(buildFrontend)
    }
    clean {
        dependsOn(cleanNpmInstall)
    }
}
